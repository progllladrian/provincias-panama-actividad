## Tupla con las provincias
provincias = ('Bocas del Toro', 'Coclé', 'Colón', 'Chiriquí', 'Darién',
			'Herrera', 'Los Santos', 'Panamá', 'Veraguas', 'Panamá Oeste')

##Listas con todos los distritos
dis_bocas = ['Almirante', 'Bocas del Toro', 'Changuinola', 'Chiriquí Grande']
dis_cocle = ['Aguadulce', 'Antón', 'La Pintada', 'Natá', 'Olá', 'Penonomé']
dis_colon = ['Chagres', 'Colón', 'Donoso', 'Portobelo', 'Santa Isabel', 'Omar Torrijos Herrea']
dis_chiriqui = ['Alanje', 'Barú', 'Boquerón', 'Boquete', 'Bugaba', 'David', 'Dolega', 'Gualaca', 'Remedios', 'Renacimiento', 'San Félix', 'San Lorenzo', 'Tierras Altas', 'Tolé']
dis_darien = ['Chepigana', 'Pinogana', 'Santa Fe']
dis_herrera = ['Chitré', 'Las Minas', 'Los Pozos', 'Ocú', 'Parita', 'Pesé', 'Santa María']
dis_los_santos= ['Guararé', 'Las Tablas', 'Los Santos', 'Macaracas', 'Pedasí', 'Pocrí', 'Tonosí']
dis_panama = ['Balboa', 'Chepo', 'Chimán', 'Panamá', 'San Miguelito', 'Taboga']
dis_veraguas = ['Atalaya', 'Calobre', 'Cañazas', 'La Mesa', 'Las Palmas', 'Mariato', 'Montijo', 'Río de Jesús', 'San Francisco', 'Santa Fe', 'Santiago', 'Soná']
dis_panama_oeste = ['Arraiján', 'Capira', 'Chame', 'La Chorrera', 'San Carlos']

## Diccionario que relaciona provincias y distritos
dic_distritos = {
"Bocas del Toro" : dis_bocas,
"Coclé" : dis_cocle,			
"Colón" : dis_colon,		
"Chiriquí" : dis_chiriqui,			
"Darién" : dis_darien,			
"Herrera" : dis_herrera,			
"Los Santos" : dis_los_santos,			
"Panamá" : dis_panama,			
"Veraguas" : dis_veraguas,			
"Panamá Oeste" : dis_panama_oeste
}			

salir = 0
while (salir == 0):			
	print(""" -- Provincias de Panamá -- 
1 - Mostrar provincias
2 - Agregar distrito
3 - Eliminar distrito
4 - Mostrar distritos según provincia
5 - Salir""")

	try:
		op = int(input("Opción escogida: "))
	except:
		print("Error: introduzca una opción válida.")
	else:
		print("\n")
		if (op == 1):
			for x in provincias:
				print(x)
		if (op == 2):
			error = 1
			while (error == 1):
				try:
					provincia = input("Provincia: ")
					distrito = input("Nuevo distrito: ")
					dic_distritos.get(provincia).append(distrito)
				except:
					print("Error: esa provincia no existe.")
				else:
					error = 0		
		if (op == 3):
			error = 1
			while (error == 1):
				try:
					provincia = input("Provincia: ")
					distrito = input("Distrito: ")
					dic_distritos.get(provincia).remove(distrito)
				except:
					print("Error: esa provincia o distrito no existe.")
				else:
					error = 0
		if (op == 4):
			error = 1
			while (error == 1):
				try:
					provincia = input("Provincia: ")
					for distrito in dic_distritos[provincia]:
						print(distrito)
				except:
					print("Error: esa provincia no existe.")
				else:
					error = 0
		if (op == 5):
			salir = 1
		if (op < 1 or op > 5):
			print("Error: introduzca una opción válida.")
		print("\n")		
		
		